import React, { useState } from "react";

const inicial_data = {
  theme: "ligth",
};

const ThemeContext = React.createContext(inicial_data);

const { Provider } = ThemeContext;

export const ThemeProviders = ({ _theme, children }) => {
  const [theme, setTheme] = useState(_theme);

  const toogle_theme = () => {
    setTheme((c) => (c === "ligth" ? "dark" : "ligth"));
  };
  return (
    <Provider
      value={{
        theme,
        toogle_theme,
      }}
    >
      {children}
    </Provider>
  );
};

export default ThemeContext;
