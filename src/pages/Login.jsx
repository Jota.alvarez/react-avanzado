import React, { useContext, useRef } from "react";
import { LoginForm } from "../components/navBar/LoginForm";
import ThemeContext from "../utils/theme.contex";

export function Login() {
  const ref = useRef(null);
  const context = useContext(ThemeContext);
  
  const login = (data) => {
    console.log(data);
  };

  let ds = false;
  if (ref.current) {
    ds = ref.current.isSubmitting;
  }

  return (
    <div className="container">
      <div className="row">
        <div className="col-6"></div>
        <div className="col-6">
          <LoginForm onSubmit={login} innerRef={ref} />
          <button
            className="btn btn-primary"
            disabled={ds}
            onClick={() => {
              if (ref.current) {
                ref.current.submitForm();
              }
              context.toogle_theme('dark')

            }}
            style={{ width: "39.7em", marginTop:'24px' }} 
            >
              Login
            </button>
        </div>
      </div>
    </div>
  );
}
