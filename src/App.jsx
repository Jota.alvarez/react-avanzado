import { Routes, Route, Link } from "react-router-dom";
import { Home } from "./pages/Home";
import { Login } from "./pages/Login";
import { NavBar } from "./components/navBar/NavBar";
import { Contact } from "./pages/Contact";
import ThemeContext from "./utils/theme.contex";
import React, { useContext, useRef } from "react";
import './App.css'




function App() {

  const context = useContext(ThemeContext);
  return (
   
      <div className={`App ${context.theme === 'dark' ? 'dark':''}`}>
        
      <NavBar />
      <div className="box">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />}/>  
          <Route path="/contact" element={<Contact/>}/>
        </Routes>
      </div>
    </div>
   
  )
}

export default App
