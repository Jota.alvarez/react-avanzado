import { Field, Form, Formik } from "formik";
export function LoginForm({ onSubmit, innerRef }) {
  const initial_data = {
    user_name: "",
    password: "",
  };

  const submit = (values, action) => {
    action.setSubmitting(true);
    onSubmit(values);
    action.resetForm();
    action.setSubmitting(false);
  };
  return (
    <Formik initialValues={initial_data} onSubmit={submit} innerRef={innerRef}>
      {({ isSubmitting }) => {
        return (
          <Form>
            <h1>Login</h1>
            <div className="form-group">
              <label htmlFor="username">Nombre de usuario</label>
              <Field
                className="form-control"
                id="username"
                type="text"
                name="user_name"
                placeholder="Ingrese su usuario"
              />
            </div>
            <div className="form-group">
              <label htmlFor="pass">Contraseña</label>
              <Field
                className="form-control"
                id="pass"
                type="password"
                name="password"
                placeholder="Ingrese su contraseña"
              />
            </div>
          </Form>
        );
      }}
    </Formik>
  );
}
