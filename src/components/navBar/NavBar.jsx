import { Link } from "react-router-dom";
import { useContext } from "react";
import ThemeContext from "../../utils/theme.contex";


export function NavBar() {

  const context = useContext(ThemeContext);

  return (
    <nav className="navbar navbar-expand-lg navbar navbar-dark bg-primary">
      <div className="container-fluid">
        <div>
        <Link className="navbar-brand" to="/">
          Home
        </Link>
        <button 
        onClick={context.toogle_theme}
        className="btn btn-primary">
          Change Mode
        </button>
        </div>
        <div className="navbar-nav">
          <Link className="nav-link active" aria-current="page" to="/login">
            Login
          </Link>
          <Link className="nav-link active" to="/contact">
            Contact
          </Link>
        </div>
      </div>
    </nav>
  );
}
